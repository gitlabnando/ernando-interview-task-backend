# Finished Recruitment Task 🙈

There's an [Insomnia](https://insomnia.rest) collection attached.

## Context

I have a limited (only theoretical) knowledge of DDD and never used professionally, so I was not comfortable implementing it fully, with VO, entities, aggregates, etc.. 

In my previous company, my team developed a microservice with DDD using NestJS, but it was the software architect that made the design and we (developers) did not had a voice, unfortunately.

So, I tried to follow the existent structure and did my best to make it in a way that made sense according to my interpretation of DDD.

## Changes

* While the task originally asked to create 2 endpoints for approval / rejection, I decided to combine them into a single one. I made one endpoint because It would make more sense in terms of a [RESTful API structure](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH) and would be much easier for the client to develop the integration. Of course, in a real-world scenario I'd debate the idea to see if it was applicable.
* Small change made to `ApprovalFacade::class`, to make the validation use the invoice current status, instead of the client sent status.
