<?php

namespace App\Domain\Repositories;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Infrastructure\Models\InvoicesModel;
use Ramsey\Uuid\UuidInterface;

interface InvoicesRepositoryInterface
{
    public function findAll(): array;
    public function changeStatus(UuidInterface $id, StatusEnum $status): false|InvoicesModel;
}
