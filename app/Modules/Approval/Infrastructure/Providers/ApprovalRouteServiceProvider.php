<?php

namespace App\Modules\Approval\Infrastructure\Providers;

use App\Domain\Enums\StatusEnum;
use App\Modules\{Approval\Api\Dto\ApprovalDto, Invoices\Infrastructure\Models\InvoicesModel};
use Illuminate\{Foundation\Support\Providers\RouteServiceProvider as ServiceProvider,
    Http\Exceptions\HttpResponseException,
    Http\JsonResponse,
    Support\Facades\Request,
    Support\Facades\Route
};
use Ramsey\Uuid\Rfc4122\UuidV4;

class ApprovalRouteServiceProvider extends ServiceProvider
{
    public function boot()
    {
        parent::boot();

        Route::bind('approvalDto', function (string $id) {
            $invoice = InvoicesModel::findOrFail($id);
            $sentStatus = Request::input('status');

            if (!StatusEnum::tryFrom($sentStatus)) {
                $validStatus = implode(', ', array_column(StatusEnum::cases(), 'name'));
                $message = "Invalid status. Only these are permitted: {$validStatus}";
                $response = new JsonResponse(['error' => $message], 404);

                throw new HttpResponseException($response);
            }

            return new ApprovalDto(
                UuidV4::fromString($invoice->id),
                StatusEnum::tryFrom($sentStatus),
                $invoice
            );
        });
    }
}
