<?php

namespace App\Modules\Invoices\Listeners;

use App\Domain\Repositories\InvoicesRepositoryInterface;
use App\Modules\Approval\Api\Events\EntityApproved;

class InvoiceApproved
{
    public function __construct(private InvoicesRepositoryInterface $repository)
    {
    }

    public function handle(EntityApproved $event): void
    {
        $this->repository->changeStatus($event->approvalDto->id, $event->approvalDto->status);
    }
}
