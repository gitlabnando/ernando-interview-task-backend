<?php

namespace App\Modules\Invoices\Listeners;

use App\Domain\Repositories\InvoicesRepositoryInterface;
use App\Modules\Approval\Api\Events\EntityRejected;

class InvoiceRejected
{
    public function __construct(private InvoicesRepositoryInterface $repository)
    {
    }

    public function handle(EntityRejected $event): void
    {
        $this->repository->changeStatus($event->approvalDto->id, $event->approvalDto->status);
    }
}
