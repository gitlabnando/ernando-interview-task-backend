<?php

namespace App\Modules\Invoices\UserInterface;

use App\Infrastructure\Controller;
use App\Modules\{Approval\Api\Dto\ApprovalDto, Invoices\Application\InvoicesApplication};
use Illuminate\Http\{JsonResponse, Response};
use Exception;
use LogicException;

class InvoicesController extends Controller
{
    public function __construct(private InvoicesApplication $application)
    {
    }

    public function index(): JsonResponse
    {
        try {
            $invoices = $this->application->list();

            return $this->respondSucessfully($invoices);
        } catch (Exception $e) {
            return $this->respondWithError(
                $e->getMessage(),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function changeStatus(ApprovalDto $dto): JsonResponse
    {
        try {
            $this->application->changeStatus($dto);

            return $this->respondSucessfully();
        } catch (LogicException $e) {
            return $this->respondWithError(
                $e->getMessage(),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    private function respondSucessfully(array $data = []): JsonResponse
    {
        $response = [
            'success' => true,
            'data' => $data
        ];

        return response()->json($response);
    }

    private function respondWithError(string $message, int $code): JsonResponse
    {
        $response = [
            'success' => false,
            'message' => $message,
        ];

        return response()->json($response, $code);
    }
}
