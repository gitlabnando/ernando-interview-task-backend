<?php

namespace App\Modules\Invoices\Application;

use App\Domain\{Enums\StatusEnum, Repositories\InvoicesRepositoryInterface};
use App\Modules\Approval\Api\{ApprovalFacadeInterface, Dto\ApprovalDto};

class InvoicesApplication
{
    public function __construct(private InvoicesRepositoryInterface $repository)
    {
    }

    public function list(): array
    {
        return $this->repository->findAll();
    }

    public function changeStatus(ApprovalDto $dto)
    {
        $facade = app()->make(ApprovalFacadeInterface::class);

        if ($dto->status->value === StatusEnum::REJECTED) {
            return $facade->reject($dto);
        }

        return $facade->approve($dto);
    }
}
