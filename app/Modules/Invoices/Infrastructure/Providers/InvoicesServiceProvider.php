<?php

namespace App\Modules\Invoices\Infrastructure\Providers;

use App\Domain\Repositories\InvoicesRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Repositories\InvoicesRepository;
use Illuminate\Support\ServiceProvider;

class InvoicesServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(InvoicesRepositoryInterface::class, InvoicesRepository::class);
    }
}
