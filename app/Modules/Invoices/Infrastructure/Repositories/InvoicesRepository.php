<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Repositories;

use App\Domain\Enums\StatusEnum;
use App\Domain\Repositories\InvoicesRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Models\InvoicesModel;
use Ramsey\Uuid\UuidInterface;

class InvoicesRepository implements InvoicesRepositoryInterface
{
    public function __construct(private InvoicesModel $eloquent)
    {
    }

    public function findAll(): array
    {
        return $this->eloquent->with(['products', 'company'])->get()->toArray();
    }

    public function changeStatus(UuidInterface $id, StatusEnum $status): false|InvoicesModel
    {
        $invoice = $this->eloquent->find($id);

        return tap($invoice)->update([
            'status' => $status->value,
        ]);
    }
}
