<?php

namespace App\Modules\Invoices\Infrastructure\Models;

use App\Modules\{Companies\Infrastructure\CompaniesModel, Products\Infrastructure\Models\ProductsModel};
use Illuminate\Database\Eloquent\{Model, Relations\BelongsTo, Relations\BelongsToMany};

class InvoicesModel extends Model
{
    protected $table = 'invoices';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $fillable = [
        'number',
        'date',
        'due_date',
        'company_id',
        'status',
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(CompaniesModel::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(
            ProductsModel::class,
            'invoice_product_lines',
            'invoice_id',
            'product_id');
    }
}
