<?php

namespace App\Modules\Products\Infrastructure\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceProductLinesModel extends Model
{
    protected $table = 'invoice_product_lines';
    protected $keyType = 'string';
}
