<?php

namespace App\Modules\Products\Infrastructure\Models;

use App\{InvoiceProductLine, Modules\Invoices\Infrastructure\Models\InvoicesModel};
use Illuminate\Database\Eloquent\Model;

class ProductsModel extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $fillable = ['name', 'street', 'city', 'zip', 'phone', 'email'];

    public function invoices()
    {
        return $this->belongsToMany(
            InvoicesModel::class,
            'invoice_product_lines',
            'product_id',
            'invoice_id'
        );
    }
}
