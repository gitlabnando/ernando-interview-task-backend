<?php

namespace App\Modules\Companies\Infrastructure;

use App\Modules\Invoices\Infrastructure\Models\InvoicesModel;
use Illuminate\Database\Eloquent\Model;

class CompaniesModel extends Model
{
    protected $table = 'companies';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $fillable = ['name', 'street', 'city', 'zip', 'phone', 'email'];

    public function invoices()
    {
        return $this->hasMany(InvoicesModel::class);
    }
}
